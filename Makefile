.POSIX:
.SUFFIXES:

HARE = hare
PREFIX = /usr/local
SRCDIR = $(PREFIX)/src
BINDIR = $(PREFIX)/bin
HARESRCDIR = $(SRCDIR)/hare
THIRDPARTYDIR = $(HARESRCDIR)/third-party

dnscrypt-client:
	$(HARE) build $(HAREFLAGS) -o $@ cmd/dnscrypt-client

clean:
	rm -f dnscrypt-client

install-client: dnscrypt-client
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f dnscrypt-client $(DESTDIR)$(BINDIR)/

install-module:
	mkdir -p $(DESTDIR)$(THIRDPARTYDIR)/net/dnscrypt
	cp -f net/dnscrypt/* $(DESTDIR)$(THIRDPARTYDIR)/net/dnscrypt/

install: install-client install-module

uninstall:
	rm -rf $(DESTDIR)$(THIRDPARTYDIR)/net/dnscrypt
	rm -f  $(DESTDIR)$(BINDIR)/dnscrypt-client
