# hare-dnscrypt

This package implements [DNSCrypt protocol][protocol]

## Status

- **Client**: Kind of usable
- **Server**: Not started

## Dependencies

- [hare-dnsstamp](https://codeberg.org/illiliti/hare-dnsstamp)
- [hare-iouring](https://codeberg.org/illiliti/hare-iouring)
- [hare-fastdns](https://codeberg.org/illiliti/hare-fastdns)

## Planned features

- XSalsa20Poly1305 support
- Multiple servers
- Proper logging
- Graceful shutdown

## Non-goals

- Load-balancing/latency measuring
- Systemd integration
- Any kind of filtering
- Any kind of rules
- Other protocols

[protocol]: https://dnscrypt.info/protocol
