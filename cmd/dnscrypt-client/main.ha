// License: MPL-2.0
// (c) 2023 illiliti <illiliti@protonmail.com>
use encoding::dnsstamp;
use net::dnscrypt;
use getopt;
use net::udp;
use net::tcp;
use linux::iouring;
use rt;
use net::ip;
use time;
use os;
use net;
use fmt;
use endian;

def LISTENER_UDPSOCK: iouring::fixed = 0;
def LISTENER_TCPSOCK: iouring::fixed = 1;
def TIMER_FILE: iouring::fixed = 2;
def CACHE_SIZE: size = 1024;
def CACHE_MINTTL: u32 = 3600;
def BUF_GID: iouring::bufgid = 0;
def BUF_SIZE: u32 = 4096;
def SQE_CNT: u32 = 512;
def CQE_CNT: u32 = SQE_CNT * 2;
def FILE_CNT: u32 = CQE_CNT;
def BUF_CNT: u32 = CQE_CNT * 2;

type event = enum u8 {
	UNHANDLED = 0,

	CLIENT_UDPQUERY,
	RESOLVER_UDPREPLY,
	FALLBACK_TCPREPLY,

	CLIENT_TCPCONN,
	CLIENT_TCPQUERY,
	RESOLVER_TCPREPLY,

	CERTIFICATE_UDPFETCH,
	CERTIFICATE_UDPREPLY,

	CERTIFICATE_TCPFETCH,
	CERTIFICATE_TCPREPLY,

	RECYCLE_BUFFER,
};

type userdata = struct {
	event: event,
	reslid: u8,
	certid: u8,
	flags: u8,
	union {
		sock: iouring::fixed,
		struct {
			bufid: u16,
			payloadsz: u16,
		},
	},
};
static assert(size(userdata) <= size(iouring::userdata));

type fallback = void;

let secretkey: [32]u8 = [0...];
let publickey: [32]u8 = [0...];
@init fn initkeypair() void = {
	dnscrypt::initkeypair(&secretkey, &publickey);
};

let hour_timeout = rt::timespec { ... };
let threesec_timeout = rt::timespec { ... };
@init fn inittimeout() void = {
	time::duration_to_timespec(time::HOUR * 1, &hour_timeout);
	time::duration_to_timespec(time::SECOND * 3, &threesec_timeout);
};

export fn main() void = {
	const help: [_]getopt::help = [
		"dnscrypt client/proxy",
		('s', "stamp", "DNS Stamp of DNSCrypt resolver"),
	];
	let cmd = getopt::parse(os::args, help...);
	defer getopt::finish(&cmd);

	let stamp = "";
	for (let i: size = 0; i < len(cmd.opts); i += 1) {
		const (flag, param) = cmd.opts[i];
		switch (flag) {
		case 's' =>
			stamp = param;
		case =>
			abort();
		};
	};

	let resl = match (newresolver(stamp)) {
	case let err: error =>
		fmt::fatal("Error setting up DNSCrypt resolver:", strerror(err));
	case let resl: resolver =>
		yield resl;
	};
	defer resolver_finish(&resl);

	// TODO: let user change size and minttl
	let cache = match (newcache(CACHE_SIZE, CACHE_MINTTL)) {
	case let err: error =>
		fmt::fatal("Error setting up DNS cache:", strerror(err));
	case let cache: cache =>
		yield cache;
	};
	defer cache_finish(&cache);

	// TODO: add flag to force either udp or tcp
	// TODO: let user change ip:port
	let listener_udpsock = match (udp::listen(ip::LOCAL_V6, 53, udp::reuseaddr)) {
	case let sock: net::socket =>
		yield sock;
	case let err: net::error =>
		fmt::fatal("Error binding to address:", net::strerror(err));
	};
	defer net::close(listener_udpsock)!;

	let listener_tcpsock = match (tcp::listen(ip::LOCAL_V6, 53, tcp::reuseaddr)) {
	case let sock: net::socket =>
		yield sock;
	case let err: net::error =>
		fmt::fatal("Error binding to address:", net::strerror(err));
	};
	defer net::close(listener_tcpsock)!;

	let timer_file = match (rt::timerfd_create(rt::CLOCK_MONOTONIC, 0)) {
	case let fd: int =>
		yield fd;
	case let err: rt::errno =>
		fmt::fatal("Error creating timerfd:", rt::strerror(err));
	};
	defer rt::close(timer_file)!;

	const ts = rt::itimerspec {
		it_interval = hour_timeout,
		it_value = rt::timespec { tv_sec = 0, tv_nsec = 1 },
	};
	rt::timerfd_settime(timer_file, 0, &ts, null)!;

	let iouring = match (iouring::new(SQE_CNT,
		CQE_CNT: iouring::cqsize,
		iouring::submitall,
		iouring::cooptaskrun,
		iouring::singleissuer,
		iouring::taskrunflag,
		iouring::defertaskrun,
	)) {
	case let err: iouring::error =>
		fmt::fatal("Error setting up io_uring instance:", iouring::strerror(err));
	case let iouring: iouring::iouring =>
		yield iouring;
	};
	defer iouring::finish(&iouring);

	// XXX: https://lists.sr.ht/~sircmpwn/hare-dev/%3C20230328051323.jmqjlsxiw6fv32m3%40KISS%3E
	// const files: [_]int = [
	// 	[LISTENER_UDPSOCK] = listener_udpsock,
	//	[LISTENER_TCPSOCK] = listener_tcpsock,
	//	[TIMER_FILE] = timer_file,
	// ];
	let files: [3]int = [-1...];
	files[LISTENER_UDPSOCK] = listener_udpsock;
	files[LISTENER_TCPSOCK] = listener_tcpsock;
	files[TIMER_FILE] = timer_file;
	match (iouring::registerfiles_sparse(&iouring, len(files): u32 + FILE_CNT)) {
	case let err: iouring::error =>
		fmt::fatal("Error reserving space for io_uring direct descriptors:", iouring::strerror(err));
	case void =>
		void;
	};
	match (iouring::registerfiles_update(&iouring, 0, files)) {
	case let err: iouring::error =>
		fmt::fatal("Error registering io_uring direct descriptors:", iouring::strerror(err));
	case void =>
		void;
	};

	let bufring = match (iouring::newbufring(&iouring, BUF_GID, BUF_SIZE, BUF_CNT)) {
	case let err: iouring::error =>
		fmt::fatal("Error setting up io_uring buffer ring:", iouring::strerror(err));
	case let bufring: iouring::bufring =>
		yield bufring;
	};
	defer iouring::bufring_finish(&bufring);

	prepare_certificatetimer(&iouring)!;
	iouring::submitwait(&iouring, 1)!;
	const cqe = iouring::dequeuecqe(&iouring);
	prepare_certificateudpfetch(&iouring, &bufring, &resl, &cqe)!;
	// XXX: function pointer would be better
	let isfallback = false;
	for (true) {
		iouring::submitwait(&iouring, 1)!;
		const cqe = iouring::dequeuecqe(&iouring);
		const ret = if (!isfallback) {
			yield consume_certificateudpreply(&iouring, &bufring, &resl, &cqe);
		} else {
			yield consume_certificatetcpreply(&iouring, &bufring, &resl, &cqe);
		};
		match (ret) {
		case let err: error =>
			fmt::fatal("Error fetching certificate from DNSCrypt resolver:", strerror(err));
		case fallback =>
			assert(!isfallback);
			isfallback = true;
		case void =>
			break;
		};
	};

	// XXX: remove this: https://github.com/axboe/liburing/issues/851
	prepare_certificatetimer(&iouring)!;

	prepare_querylistener(&iouring)!;
	for (true) match (iouring::submitwait(&iouring, 1)) {
	case u32 =>
		let iter = iouring::itercq(&iouring);
		defer iouring::cqiter_finish(&iter);
		for (true) match (iouring::cqiter_next(&iter)) {
		case let cqe: *iouring::cqe =>
			match (handle_event(&iouring, &bufring, &cache, &resl, cqe)) {
			case let err: error =>
				const user = &iouring::cqe_getuser(cqe): *userdata;
				fmt::errorf("Error processing event {}: {}\n", user.event: u8, strerror(err))!;
			case void =>
				break;
			};
		case void =>
			break;
		};
	case let err: iouring::error =>
		fmt::fatal("Error polling io_uring for events:", iouring::strerror(err));
	};
};

fn handle_event(iouring: *iouring::iouring, bufring: *iouring::bufring, cache: *cache,
		resl: *resolver, cqe: *iouring::cqe) (void | error) = {
	const user = &iouring::cqe_getuser(cqe): *userdata;
	switch (user.event) {
	case event::CLIENT_UDPQUERY =>
		prepare_resolverudpquery(iouring, bufring, cache, resl, cqe)?;
	case event::CLIENT_TCPCONN =>
		prepare_clienttcpquery(iouring, bufring, cqe)?;
	case event::CLIENT_TCPQUERY =>
		prepare_resolvertcpquery(iouring, bufring, cache, resl, cqe)?;
	case event::RESOLVER_UDPREPLY =>
		prepare_clientudpresponse(iouring, bufring, cache, resl, cqe)?;
	case event::FALLBACK_TCPREPLY =>
		prepare_fallbackudpresponse(iouring, bufring, cache, resl, cqe)?;
	case event::RESOLVER_TCPREPLY =>
		prepare_clienttcpresponse(iouring, bufring, cache, resl, cqe)?;
	case event::CERTIFICATE_UDPFETCH =>
		prepare_certificateudpfetch(iouring, bufring, resl, cqe)?;
	case event::CERTIFICATE_TCPFETCH =>
		prepare_certificatetcpfetch(iouring, bufring, resl, cqe)?;
	case event::CERTIFICATE_UDPREPLY =>
		consume_certificateudpreply(iouring, bufring, resl, cqe)?;
	case event::CERTIFICATE_TCPREPLY =>
		consume_certificatetcpreply(iouring, bufring, resl, cqe)?;
	case event::RECYCLE_BUFFER =>
		iouring::bufring_recyclebuf(bufring, user.bufid);
	case event::UNHANDLED =>
		iouring::cqe_getresult(cqe)?;
		abort("Unhandled event");
	case =>
		abort("Unknown event");
	};
};

fn prepare_certificatetimer(iouring: *iouring::iouring) (void | error) = {
	const data = userdata {
		event = event::CERTIFICATE_UDPFETCH,
		...
	};
	const sqe = iouring::read(TIMER_FILE, null, size(u64), 0, BUF_GID, touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;
};

fn prepare_querylistener(iouring: *iouring::iouring) (void | error) = {
	static let msghdr = rt::msghdr {
		msg_namelen = size(rt::sockaddr): u32,
		...
	};
	const data = userdata {
		event = event::CLIENT_UDPQUERY,
		...
	};
	const sqe = iouring::recvmsg(LISTENER_UDPSOCK, &msghdr, 0, iouring::recv_flag::MULTISHOT,
		iouring::recv_flag::POLL_FIRST, BUF_GID, touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	const data = userdata {
		event = event::CLIENT_TCPCONN,
		...
	};
	const sqe = iouring::accept(LISTENER_TCPSOCK, null, null, 0, iouring::accept_flag::MULTISHOT,
		touserdata(&data), iouring::FILE_INDEX_ALLOC);
	iouring::enqueuesqe(iouring, &sqe)?;
};

fn prepare_clienttcpquery(iouring: *iouring::iouring, bufring: *iouring::bufring,
		cqe: *iouring::cqe) (void | error) = {
	const sock = iouring::cqe_getresult(cqe)?: iouring::fixed;
	// HACK: need errdefer
	let success = false;
	defer if (!success) closefixed(iouring, sock);
	const data = userdata {
		event = event::CLIENT_TCPQUERY,
		sock = sock,
		...
	};
	const sqe = iouring::recv(sock, null, 0, rt::MSG_WAITALL, iouring::link, BUF_GID,
		iouring::recv_flag::POLL_FIRST, touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::linktimeout(&threesec_timeout, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	success = true;
};

fn prepare_certificateudpfetch(iouring: *iouring::iouring, bufring: *iouring::bufring,
		resl: *resolver, cqe: *iouring::cqe) (void | error) = {
	iouring::cqe_getresult(cqe)?;
	const bufid = iouring::cqe_getbufid(cqe);
	// HACK: need errdefer
	let success = false;
	defer if (!success) iouring::bufring_recyclebuf(bufring, bufid);

	const buf = iouring::bufring_getbuf(bufring, bufid);
	const payloadsz = dnscrypt::certificate_encodequery(buf, resl.stamp.provider);
	const payload = buf[..payloadsz];

	const family = resl.sockaddr.in.sin_family: int;
	// TODO: use direct descriptor(need bitmap)
	let sock = rt::socket(family, rt::SOCK_DGRAM, 0)?;
	defer if (!success) rt::close(sock)!;

	const sqe = iouring::connect(sock, &resl.sockaddr, size(rt::sockaddr), iouring::link, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::send(sock, &payload[0], len(payload), 0, iouring::link, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const data = userdata {
		event = event::CERTIFICATE_UDPREPLY,
		bufid = bufid,
		payloadsz = payloadsz,
		...
	};
	const sqe = iouring::recv(sock, null, 0, 0, iouring::hardlink, BUF_GID,
		iouring::recv_flag::POLL_FIRST, touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::linktimeout(&threesec_timeout, iouring::hardlink, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::close(sock, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	success = true;
};

fn prepare_certificatetcpfetch(iouring: *iouring::iouring, bufring: *iouring::bufring,
		resl: *resolver, cqe: *iouring::cqe) (void | error) = {
	iouring::cqe_getresult(cqe)?;
	const bufid = iouring::cqe_getbufid(cqe);
	// HACK: need errdefer
	let success = false;
	defer if (!success) iouring::bufring_recyclebuf(bufring, bufid);

	const buf = iouring::bufring_getbuf(bufring, bufid);
	const payloadsz = dnscrypt::certificate_encodequery(buf[size(u16)..], resl.stamp.provider);
	endian::beputu16(buf[..size(u16)], payloadsz);
	const payload = buf[..size(u16) + payloadsz];

	const family = resl.sockaddr.in.sin_family: int;
	// TODO: use direct descriptor(need bitmap)
	let sock = rt::socket(family, rt::SOCK_STREAM, 0)?;
	defer if (!success) rt::close(sock)!;

	const sqe = iouring::connect(sock, &resl.sockaddr, size(rt::sockaddr),
		iouring::link, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const data = userdata {
		event = event::RECYCLE_BUFFER,
		bufid = bufid,
		...
	};
	const sqe = iouring::send(sock, &payload[0], len(payload), rt::MSG_WAITALL, iouring::link, touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	const data = userdata {
		event = event::CERTIFICATE_TCPREPLY,
		...
	};
	const sqe = iouring::recv(sock, null, 0, rt::MSG_WAITALL, iouring::hardlink, BUF_GID,
		iouring::recv_flag::POLL_FIRST, touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::linktimeout(&threesec_timeout, iouring::hardlink, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::close(sock, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	success = true;
};

fn prepare_fallbacktcpfetch(iouring: *iouring::iouring, bufring: *iouring::bufring,
		resl: *resolver, cqe: *iouring::cqe) (fallback | error) = {
	const user = &iouring::cqe_getuser(cqe): *userdata;
	// HACK: need errdefer
	let success = false;
	defer if (!success) iouring::bufring_recyclebuf(bufring, user.bufid);

	let payload = iouring::bufring_getbuf(bufring, user.bufid)[..user.payloadsz];
	let payloadsz: [size(u16)]u8 = [0...];
	endian::beputu16(payloadsz, user.payloadsz);
	// FIXME: inefficient
	static insert(payload[0], payloadsz...);

	const family = resl.sockaddr.in.sin_family: int;
	// TODO: use direct descriptor(need bitmap)
	let sock = rt::socket(family, rt::SOCK_STREAM, 0)?;
	defer if (!success) rt::close(sock)!;

	const sqe = iouring::connect(sock, &resl.sockaddr, size(rt::sockaddr),
		iouring::link, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const data = userdata {
		event = event::RECYCLE_BUFFER,
		bufid = user.bufid,
		...
	};
	const sqe = iouring::send(sock, &payload[0], len(payload), rt::MSG_WAITALL, iouring::link,
		touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	const data = userdata {
		event = event::CERTIFICATE_TCPREPLY,
		...
	};
	const sqe = iouring::recv(sock, null, 0, rt::MSG_WAITALL, iouring::hardlink, BUF_GID,
		iouring::recv_flag::POLL_FIRST, touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::linktimeout(&threesec_timeout, iouring::hardlink, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::close(sock, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	success = true;
	return fallback;
};

fn consume_certificateudpreply(iouring: *iouring::iouring, bufring: *iouring::bufring,
		resl: *resolver, cqe: *iouring::cqe) (void | fallback | error) = {
	// XXX: remove this: https://github.com/axboe/liburing/issues/851
	defer prepare_certificatetimer(iouring)!;

	const payloadsz = match (iouring::cqe_getresult(cqe)) {
	case let payloadsz: u32 =>
		yield payloadsz;
	case let err: rt::errno =>
		// cancelled by link_timeout op
		if (err == rt::ECANCELED) {
			return prepare_fallbacktcpfetch(iouring, bufring, resl, cqe);
		} else {
			return err;
		};
	case let err: iouring::error =>
		return err;
	};
	const user = &iouring::cqe_getuser(cqe): *userdata;
	defer iouring::bufring_recyclebuf(bufring, user.bufid);
	const bufid = iouring::cqe_getbufid(cqe);
	defer iouring::bufring_recyclebuf(bufring, bufid);
	const payload = iouring::bufring_getbuf(bufring, bufid)[..payloadsz];
	const stampkey = resl.stamp.publickey[..32]: *[32]u8;
	const curcert = resolver_getcert(resl, resl.certid);
	const newcert = dnscrypt::certificate_decoderesponse(payload, stampkey, curcert)?;
	resolver_switchcert(resl, &newcert, &secretkey);
};

fn consume_certificatetcpreply(iouring: *iouring::iouring, bufring: *iouring::bufring,
		resl: *resolver, cqe: *iouring::cqe) (void | error) = {
	// XXX: remove this: https://github.com/axboe/liburing/issues/851
	defer prepare_certificatetimer(iouring)!;

	if (iouring::cqe_getresult(cqe)? == 0) {
		assert(!iouring::cqe_hasbufid(cqe));
		return unexpected_eof;
	};
	const bufid = iouring::cqe_getbufid(cqe);
	defer iouring::bufring_recyclebuf(bufring, bufid);
	const buf = iouring::bufring_getbuf(bufring, bufid);
	const payloadsz = endian::begetu16(buf[..size(u16)]);
	const payload = buf[size(u16)..size(u16) + payloadsz];
	const stampkey = resl.stamp.publickey[..32]: *[32]u8;
	const curcert = resolver_getcert(resl, resl.certid);
	const newcert = dnscrypt::certificate_decoderesponse(payload, stampkey, curcert)?;
	resolver_switchcert(resl, &newcert, &secretkey);
};

fn prepare_resolverudpquery(iouring: *iouring::iouring, bufring: *iouring::bufring, cache: *cache,
		resl: *resolver, cqe: *iouring::cqe) (void | error) = {
	const payloadsz = iouring::cqe_getresult(cqe)?;
	const bufid = iouring::cqe_getbufid(cqe);
	// HACK: need errdefer
	let success = false;
	defer if (!success) iouring::bufring_recyclebuf(bufring, bufid);

	const buf = iouring::bufring_getbuf(bufring, bufid);
	static const payloadstart = size(rt::io_uring_recvmsg_out) + size(rt::sockaddr);
	let payload = buf[payloadstart..payloadsz];
	match (cache_lookup(cache, &payload)?) {
	case void =>
		static const clientaddrstart = size(rt::io_uring_recvmsg_out);
		const clientaddr = &buf[clientaddrstart]: *rt::sockaddr;
		const data = userdata {
			event = event::RECYCLE_BUFFER,
			bufid = bufid,
			...
		};
		const sqe = iouring::sendto(LISTENER_UDPSOCK, &payload[0], len(payload), 0,
			clientaddr, size(rt::sockaddr), touserdata(&data));
		iouring::enqueuesqe(iouring, &sqe)?;
		success = true;
		return;
	case nocache =>
		void;
	};
	const cert = resolver_getcert(resl, resl.certid);
	dnscrypt::encryptquery(&payload, &cert.sharedkey, &publickey, cert);

	const family = resl.sockaddr.in.sin_family: int;
	// TODO: use direct descriptor(need bitmap)
	let sock = rt::socket(family, rt::SOCK_DGRAM, 0)?;
	defer if (!success) rt::close(sock)!;

	const sqe = iouring::connect(sock, &resl.sockaddr, size(rt::sockaddr), iouring::link, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::send(sock, &payload[0], len(payload), 0, iouring::link, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const data = userdata {
		event = event::RESOLVER_UDPREPLY,
		certid = resl.certid,
		bufid = bufid,
		payloadsz = len(payload): u16,
		...
	};
	const sqe = iouring::recv(sock, null, 0, 0, iouring::hardlink, BUF_GID,
		iouring::recv_flag::POLL_FIRST, touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::linktimeout(&threesec_timeout, iouring::hardlink, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::close(sock, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	success = true;
};

fn prepare_resolvertcpquery(iouring: *iouring::iouring, bufring: *iouring::bufring, cache: *cache,
		resl: *resolver, cqe: *iouring::cqe) (void | error) = {
	const user = &iouring::cqe_getuser(cqe): *userdata;
	// HACK: need errdefer
	let success = false;
	defer if (!success) closefixed(iouring, user.sock);

	if (iouring::cqe_getresult(cqe)? == 0) {
		assert(!iouring::cqe_hasbufid(cqe));
		return unexpected_eof;
	};
	const bufid = iouring::cqe_getbufid(cqe);
	defer if (!success) iouring::bufring_recyclebuf(bufring, bufid);

	const buf = iouring::bufring_getbuf(bufring, bufid);
	const payloadsz = endian::begetu16(buf[..size(u16)]);
	let payload = buf[size(u16)..size(u16) + payloadsz];
	match (cache_lookup(cache, &payload)?) {
	case void =>
		endian::beputu16(buf[..size(u16)], len(payload): u16);
		const resp = buf[..size(u16) + len(payload)];
		const data = userdata {
			event = event::RECYCLE_BUFFER,
			bufid = bufid,
			...
		};
		const sqe = iouring::send(user.sock, &resp[0], len(resp), rt::MSG_WAITALL,
			touserdata(&data), iouring::hardlink);
		iouring::enqueuesqe(iouring, &sqe)?;

		const sqe = iouring::close(user.sock, iouring::skipcqe);
		iouring::enqueuesqe(iouring, &sqe)?;

		success = true;
		return;
	case nocache =>
		void;
	};
	const cert = resolver_getcert(resl, resl.certid);
	dnscrypt::encryptquery(&payload, &cert.sharedkey, &publickey, cert);
	endian::beputu16(buf[..size(u16)], len(payload): u16);
	const query = buf[..size(u16) + len(payload)];

	const family = resl.sockaddr.in.sin_family: int;
	// TODO: use direct descriptor(need bitmap)
	let sock = rt::socket(family, rt::SOCK_STREAM, 0)?;
	defer if (!success) rt::close(sock)!;

	const sqe = iouring::connect(sock, &resl.sockaddr, size(rt::sockaddr),
		iouring::link, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const data = userdata {
		event = event::RECYCLE_BUFFER,
		bufid = bufid,
		...
	};
	const sqe = iouring::send(sock, &query[0], len(query), rt::MSG_WAITALL, iouring::link,
		touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	const data = userdata {
		event = event::RESOLVER_TCPREPLY,
		certid = resl.certid,
		sock = user.sock,
		...
	};
	const sqe = iouring::recv(sock, null, 0, rt::MSG_WAITALL, iouring::hardlink, BUF_GID,
		iouring::recv_flag::POLL_FIRST, touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::linktimeout(&threesec_timeout, iouring::hardlink, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::close(sock, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	success = true;
};

fn prepare_fallbacktcpquery(iouring: *iouring::iouring, bufring: *iouring::bufring, cache: *cache,
		resl: *resolver, cqe: *iouring::cqe) (fallback | error) = {
	const user = &iouring::cqe_getuser(cqe): *userdata;
	// HACK: need errdefer
	let success = false;
	defer if (!success) iouring::bufring_recyclebuf(bufring, user.bufid);

	static const payloadstart = size(rt::io_uring_recvmsg_out) + size(rt::sockaddr);
	let payload = iouring::bufring_getbuf(bufring, user.bufid)[
		payloadstart..payloadstart + user.payloadsz];
	let payloadsz: [size(u16)]u8 = [0...];
	endian::beputu16(payloadsz, user.payloadsz);
	// FIXME: inefficient
	static insert(payload[0], payloadsz...);

	const family = resl.sockaddr.in.sin_family: int;
	// TODO: use direct descriptor(need bitmap)
	let sock = rt::socket(family, rt::SOCK_STREAM, 0)?;
	defer if (!success) rt::close(sock)!;

	const sqe = iouring::connect(sock, &resl.sockaddr, size(rt::sockaddr),
		iouring::link, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::send(sock, &payload[0], len(payload), rt::MSG_WAITALL,
		iouring::link, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const data = userdata {
		event = event::FALLBACK_TCPREPLY,
		certid = user.certid,
		bufid = user.bufid,
		...
	};
	const sqe = iouring::recv(sock, null, 0, rt::MSG_WAITALL, iouring::hardlink, BUF_GID,
		iouring::recv_flag::POLL_FIRST, touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::linktimeout(&threesec_timeout, iouring::hardlink, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::close(sock, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	success = true;
	return fallback;
};

fn prepare_clientudpresponse(iouring: *iouring::iouring, bufring: *iouring::bufring,
		cache: *cache, resl: *resolver, cqe: *iouring::cqe) (void | fallback | error) = {
	const payloadsz = match (iouring::cqe_getresult(cqe)) {
	case let payloadsz: u32 =>
		yield payloadsz;
	case let err: rt::errno =>
		// cancelled by link_timeout op
		if (err == rt::ECANCELED) {
			return prepare_fallbacktcpquery(iouring, bufring, cache, resl, cqe);
		} else {
			return err;
		};
	case let err: iouring::error =>
		return err;
	};
	const user = &iouring::cqe_getuser(cqe): *userdata;
	defer iouring::bufring_recyclebuf(bufring, user.bufid);

	const bufid = iouring::cqe_getbufid(cqe);
	// HACK: need errdefer
	let success = false;
	defer if (!success) iouring::bufring_recyclebuf(bufring, bufid);

	let payload = iouring::bufring_getbuf(bufring, bufid)[..payloadsz];
	const cert = resolver_getcert(resl, user.certid);
	dnscrypt::decryptresponse(&payload, &cert.sharedkey, cert)?;
	// XXX: potential cache poisoning due to lack of domain name checking. only possible if resolver is rogue.
	cache_insert(cache, payload)?;

	const payloadsz = len(payload);
	static const clientaddrstart = size(rt::io_uring_recvmsg_out);
	const rawclientaddr = iouring::bufring_getbuf(bufring, user.bufid)[
		clientaddrstart..clientaddrstart + size(rt::sockaddr)];
	static append(payload, rawclientaddr...);
	const clientaddr = &payload[payloadsz]: *rt::sockaddr;

	const data = userdata {
		event = event::RECYCLE_BUFFER,
		bufid = bufid,
		...
	};
	const sqe = iouring::sendto(LISTENER_UDPSOCK, &payload[0], payloadsz, 0, clientaddr,
		size(rt::sockaddr), touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	success = true;
};

fn prepare_clienttcpresponse(iouring: *iouring::iouring, bufring: *iouring::bufring,
		cache: *cache, resl: *resolver, cqe: *iouring::cqe) (void | error) = {
	const user = &iouring::cqe_getuser(cqe): *userdata;
	// HACK: need errdefer
	let success = false;
	defer if (!success) closefixed(iouring, user.sock);

	if (iouring::cqe_getresult(cqe)? == 0) {
		assert(!iouring::cqe_hasbufid(cqe));
		return unexpected_eof;
	};
	const bufid = iouring::cqe_getbufid(cqe);
	defer if (!success) iouring::bufring_recyclebuf(bufring, bufid);

	const buf = iouring::bufring_getbuf(bufring, bufid);
	const payloadsz = endian::begetu16(buf[..size(u16)]);
	let payload = buf[size(u16)..size(u16) + payloadsz];
	const cert = resolver_getcert(resl, user.certid);
	dnscrypt::decryptresponse(&payload, &cert.sharedkey, cert)?;
	// XXX: potential cache poisoning due to lack of domain name checking. only possible if resolver is rogue.
	cache_insert(cache, payload)?;
	endian::beputu16(buf[..size(u16)], len(payload): u16);
	const resp = buf[..size(u16) + len(payload)];

	const data = userdata {
		event = event::RECYCLE_BUFFER,
		bufid = bufid,
		...
	};
	const sqe = iouring::send(user.sock, &resp[0], len(resp), rt::MSG_WAITALL,
		touserdata(&data), iouring::hardlink);
	iouring::enqueuesqe(iouring, &sqe)?;

	const sqe = iouring::close(user.sock, iouring::skipcqe);
	iouring::enqueuesqe(iouring, &sqe)?;

	success = true;
};

fn prepare_fallbackudpresponse(iouring: *iouring::iouring, bufring: *iouring::bufring,
		cache: *cache, resl: *resolver, cqe: *iouring::cqe) (void | error) = {
	const user = &iouring::cqe_getuser(cqe): *userdata;
	defer iouring::bufring_recyclebuf(bufring, user.bufid);

	if (iouring::cqe_getresult(cqe)? == 0) {
		assert(!iouring::cqe_hasbufid(cqe));
		return unexpected_eof;
	};
	const bufid = iouring::cqe_getbufid(cqe);
	// HACK: need errdefer
	let success = false;
	defer if (!success) iouring::bufring_recyclebuf(bufring, bufid);

	const buf = iouring::bufring_getbuf(bufring, bufid);
	const payloadsz = endian::begetu16(buf[..size(u16)]);
	let payload = buf[size(u16)..size(u16) + payloadsz];
	const cert = resolver_getcert(resl, user.certid);
	dnscrypt::decryptresponse(&payload, &cert.sharedkey, cert)?;
	// XXX: potential cache poisoning due to lack of domain name checking. only possible if resolver is rogue.
	cache_insert(cache, payload)?;

	const payloadsz = len(payload);
	static const clientaddrstart = size(rt::io_uring_recvmsg_out);
	const rawclientaddr = iouring::bufring_getbuf(bufring, user.bufid)[
		clientaddrstart..clientaddrstart + size(rt::sockaddr)];
	static append(payload, rawclientaddr...);
	const clientaddr = &payload[payloadsz]: *rt::sockaddr;

	const data = userdata {
		event = event::RECYCLE_BUFFER,
		bufid = bufid,
		...
	};
	const sqe = iouring::sendto(LISTENER_UDPSOCK, &payload[0], payloadsz, 0, clientaddr,
		size(rt::sockaddr), touserdata(&data));
	iouring::enqueuesqe(iouring, &sqe)?;

	success = true;
};

fn closefixed(iouring: *iouring::iouring, fd: iouring::fixed) void = {
	const sqe = iouring::close(fd, iouring::skipcqe);
	if (iouring::enqueuesqe(iouring, &sqe) is iouring::error) {
		iouring::registerfiles_update(iouring, fd, [-1])!;
	};
};

fn touserdata(data: *userdata) iouring::userdata = *(data: *iouring::userdata);
